from django.http import HttpResponse, HttpResponseRedirect, Http404
from django.shortcuts import render, get_object_or_404, redirect
from django.db.models import Q
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.utils import timezone
from .models import NewsPost
from .forms import ContactMessageForm
# Create your views here.


def news_list(request):
	news_list = NewsPost.objects.active()
	if request.user.is_staff or request.user.is_superuser:
		news_list = NewsPost.objects.all()
	query = request.GET.get("q")
	if query:
		news_list = news_list.filter(
			Q(title__icontains=query) |
			Q(content__icontains=query) |
			Q(user__first_name__icontains=query) |
			Q(user__last_name__icontains=query) |
			Q(image_name__icontains=query)
			).distinct()
	paginator = Paginator(news_list, 12)
	page_request_var = "page"
	page = request.GET.get(page_request_var)
	try:
		queryset = paginator.page(page)
	except PageNotAnInteger:
		# if page is not an integer, deliver the first page
		queryset = paginator.page(1)
	except EmptyPage:
		# if page is out of range, deliver last page of result
		queryset = paginator.page(paginotor.num_pages)
	
	return render(request, 'news/newslist.html', {'news': queryset, "page_request_var": page_request_var})




def news_detail(request, slug):
	instance = get_object_or_404(NewsPost, slug=slug)
	if instance.publish > timezone.now().date() or instance.draft:
		if not request.user.is_staff or not request.user.is_superuser:
			raise Http404
	return render(request, 'news/newsdetail.html', {"instance": instance})


def contact(request):
	form = ContactMessageForm(request.POST or None)
	if form.is_valid():
		message = form.save(commit=False)
		message.save()
	return render(request, 'contact.html', {"form": form})
