from __future__ import unicode_literals

from django.apps import AppConfig


class NewsBlogConfig(AppConfig):
    name = 'news_blog'
